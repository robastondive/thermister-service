package com.astondive.therm
import scala.xml._
import scala.xml.transform._

class AddChild(label:String,newChild:Node) extends RewriteRule {
	def addChild(n:Node, newChlid:Node) = n match{
	  case Elem (prefix,label,attribs,scope,child @ _*) => Elem(prefix,label,attribs,scope,child ++ newChild : _*)
	  case _ => error("oops")
	}
	
	override def transform (n:Node) = n match {
	  case n @ Elem(_,`label`,_,_,_,_*) => addChild(n,newChild)
	  case other => other
	}
}