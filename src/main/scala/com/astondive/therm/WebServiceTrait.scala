package com.astondive.therm

import akka.actor.ActorLogging
import akka.event.LoggingAdapter


import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

import org.mashupbots.socko.events.HttpResponseStatus
import org.mashupbots.socko.routes.GET
import org.mashupbots.socko.routes.HttpRequest
import org.mashupbots.socko.routes.Path
import org.mashupbots.socko.routes.PathSegments
import org.mashupbots.socko.routes.Routes
import org.mashupbots.socko.webserver.WebServer
import org.mashupbots.socko.webserver.WebServerConfig
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.pattern.ask
import akka.util.Timeout.durationToTimeout

trait WebServiceTrait 
 {

  def getContext():akka.actor.ActorContext;
  def logInfo(s:String)
  def logError(s:String)
  
    implicit val timeout:akka.util.Timeout =1 seconds
    
  val routes =  Routes({
    
    	case HttpRequest(request) => request match {
     		case GET(PathSegments("find" :: name :: Nil)) => { 
    			logInfo(s"calling task find for  :: $name ::Nil")
                val task = <task command="find" who={name}/>
    			try{   
     				val future = getContext().actorSelection("../ThermBusinessLogicActor")  ? 
    					new BusinessTask(task)
    				val res = Await.result(future, timeout.duration)
             		logInfo(s"result of type ${res.getClass.getName()}")
             		request.response.write(res.asInstanceOf[xml.Elem].toString)
             	  		
  
    			
    			}
    			
    			catch{
            	  
    			  case jucte:java.util.concurrent.TimeoutException =>{
    			  	logError("Timeout")
    			  }
            	}
             	
           	}
    		// If favicon.ico, just return a 404 because we don't have that file
    		case Path("/favicon.ico") => {
    			request.response.write(HttpResponseStatus.NOT_FOUND)
    		}
    		
    		case Path("/") =>{
    			val ap = Runtime.getRuntime().availableProcessors().toString.toLong
    			val fm = Runtime.getRuntime().freeMemory().toString.toLong/1024/1024
    			val tm = Runtime.getRuntime().totalMemory().toString.toLong/1024/1024
    			val mm = Runtime.getRuntime().maxMemory().toString.toLong/1024/1024
    			val um = tm - fm
    			
    			request.response.write(
    			<html>
                <body> 
                  <h1>Stats <i>Available Processors</i> on <i>{ap.toString}</i></h1>
                  <br/>
                  <h1>Stats <i>Free Memory</i> on <i>{fm.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Total Memory</i> on <i>{tm.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Used Memory</i> on <i>{um.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Max Memory</i> on <i>{mm.toString}M</i></h1>
                </body>
              </html>.toString    
    			)
    		}
    	}
    })

    
}