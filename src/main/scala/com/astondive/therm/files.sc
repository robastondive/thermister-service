package com.astondive.therm

import scala.io.Source

object files {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val t = new Thermister()                        //> t  : com.astondive.therm.Thermister = com.astondive.therm.Thermister@5858ba4
                                                  //| b
  def findDevice (f:java.io.File):String ={
    val s = java.io.File.separator.toCharArray()
    val a = f.getAbsolutePath().split(s(0))
  	a(a.length-2)
  }                                               //> findDevice: (f: java.io.File)String
  
  t.toXml.toString                                //> <Thermister device="10-000802824e58">30.0</Thermister>
                                                  //| res0: String = <Thermisters> 
                                                  //| 	    <Thermister device="10-000802824e58">30.0</Thermister>
                                                  //| 	    </Thermisters>
  
  def pf (f:java.io.File) ={
  	println(f.getPath());
  	println(f.getCanonicalFile());
  	println(f.getAbsolutePath());
  }                                               //> pf: (f: java.io.File)Unit
  
  var i = t.getfiles.iterator                     //> i  : Iterator[java.io.File] = non-empty iterator
  while (i.hasNext){
  	 val f = i.next
  	 println(f.getPath())
  }                                               //> \sys\bus\w1\devices\10-000802824e58\w1-slave
  
  def concat(s:String,c : Char):String = {
  	s+c
  }                                               //> concat: (s: String, c: Char)String
  var s= new String("")                           //> s  : String = ""
  for ( x <- t.getfiles)
  {
 
   println(findDevice(x))
   
   Source.fromFile(x).foreach(y => s=s+y)
     println(s.split("t=")(1).toDouble/1000.00)
   
  }                                               //> 10-000802824e58
                                                  //| 30.0
                                                  
   val line = "sdf,, sf ,ds,sdf,sdf,fsd,,sdf,,sdf,,,sdsdf,fsd,fsd,s"
                                                  //> line  : String = sdf,, sf ,ds,sdf,sdf,fsd,,sdf,,sdf,,,sdsdf,fsd,fsd,s
   val col= line.split(",").map(_.trim).map(x => if (x.length > 0) x else null ).filter(_ != null)
                                                  //> col  : Array[String] = Array(sdf, sf, ds, sdf, sdf, fsd, sdf, sdf, sdsdf, f
                                                  //| sd, fsd, s)
   
 
}