package com.astondive.therm

import java.util.concurrent.TimeoutException
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.OneForOneStrategy
import akka.actor.Props
import akka.actor.SupervisorStrategy.Restart
import akka.actor.SupervisorStrategy.Resume
import akka.actor.actorRef2Scala
import akka.util.Timeout
import akka.util.Timeout.durationToTimeout
import framboos.actor.OutPinActor
import scala.concurrent.duration.Duration;
import java.util.concurrent.TimeUnit;
import akka.actor.UntypedActor;
import akka.actor.Cancellable;
import scala.concurrent.ExecutionContext.Implicits.global

class HttpServerStart(val interface:String,val port:Int){
   override def toString():String ={ "start" }
}

class PinOutActor(val context:akka.actor.ActorContext, val pin:Int, val set:Boolean){
   val outPin = context.actorOf(Props(new OutPinActor(0)), name = toString() )
   override def toString():String ={ "outPin"+pin }
}

class PinOutChange(val pin:Int, val set:Boolean){
   override def toString():String ={ "outPin"+pin }
}

class SupervisorActor extends Actor with ActorLogging{
	
  
    val thermBusinessActor = context.actorOf(Props[BusinessLogicActor],"ThermBusinessLogicActor")
	val webService = context.actorOf(Props[WebServiceActor], "ThermWebserviceActor")
	val  outpin = Array( new PinOutActor(context,0,false),
						 new PinOutActor(context,1,false),
						 new PinOutActor(context,2,false),
						 new PinOutActor(context,3,false),
						 new PinOutActor(context,5,false),
						 new PinOutActor(context,6,false),
						 new PinOutActor(context,7,false),
						 new PinOutActor(context,8,false),
						 new PinOutActor(context,9,false))
				
	val task = <task command="scanTank" top="{Configuration.top}" middle="{Connfiguration.middle}" bottom="{Configuration.bottom}"/>
    val Tick = new BusinessTask(task)
	val bindingTimeout: Timeout = 10.second
    implicit val system = context.system
    
    val intialDelay: scala.concurrent.duration.FiniteDuration = 1.second
    val interval: scala.concurrent.duration.FiniteDuration = 60.second

   // val cancellable = system.scheduler.schedule(delay, interval) =>	{}

  //  ActorRef tickActor = system.actorOf(Props.create(Ticker.class, this));
 
    //This will schedule to send the Tick-message
//to the tickActor after 0ms repeating every 50ms
   
    val cancellable: Cancellable = context.system.scheduler.schedule(intialDelay, interval, thermBusinessActor, Tick)
	 
 	def receive = {
	    // forward all messages to the quote actor
        case pin : PinOutChange => {
            outpin(pin.pin).outPin ! pin.set
        }
        case msg : Object => if(msg.toString().compareTo("start")==0)
        {    
            thermBusinessActor ! new Load("data.cfg")
        }
        else
        {
          log.info(msg.toString())
        }    
	}	  

	override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries=1,withinTimeRange = 10 seconds){
	    case _ : ArithmeticException => Resume	    
	    case _ : TimeoutException => Restart
	    case _ : NullPointerException => Restart
	    case _ : IllegalArgumentException => Restart
	    case _ : Exception => Restart
	}
    
	log.info("Supervisor Created")
	

}