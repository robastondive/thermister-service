package com.astondive.therm

import akka.actor.{ActorSystem, Props}
import akka.dispatch.OnFailure
import akka.io.IO
import com.typesafe.config.ConfigFactory

/**
 * @author Rob Aston-Dive
 * 
 * This is a simple boot loading object to start the supervisor actor
 *
 */
object Boot extends App {
    val system = ActorSystem("therm-astondive-com")
    val master = system.actorOf(Props[SupervisorActor],name="master")
    master ! "start"
  
}
