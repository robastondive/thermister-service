package com.astondive.therm
import scala.xml._
import scala.io.Source._
import scala.annotation.tailrec
import scala.xml.transform._
import java.util.Date
import java.util.Calendar

class Schedule (filename:String){
    
	def nodeToMin(n:Node): Int = {
		val min = ( n \ "@minute").text
		val hr = ( n \ "@hour").text
		val day = ( n \ "@day").text
		((day.toInt * 24) + hr.toInt) *60 + min.toInt
	}
	
	def closestToNow(closest:Node, next:Node, now:Date):Node = {
		val m1 = nodeToMin(closest);
		val m2 = nodeToMin(next);
		val nowMin = (now.getDay()*24 + now.getHours())*60 + now.getMinutes()
		if ((m2 > nowMin) && (m2 < m1 ))  next else closest 
	}     
	
	def lowest(sx:Seq[Node]): Node = {
		def getTemp(n:Node): Double = n.text.toDouble

		def getLowest(lowest:Node, next:Node):Node = {
			val m1 = nodeToMin(lowest);
			val m2 = nodeToMin(next);
			if (m2 < m1 ) next else lowest 
		}
			
		@tailrec def lowestAcc(lowest :Node, sx:Seq[Node]): Node = {
			if (sx.isEmpty) lowest else lowestAcc( getLowest(lowest,sx.head), sx.tail)	
		}
    
		lowestAcc(sx.head, sx.tail)
	}    
  
	def closest(sx:Seq[Node]): Double = {
		def getTemp(n:Node): Double = { n.text.toDouble }

		def closestToNow(closest:Node, next:Node, now:Date):Node = {
			val m1 = nodeToMin(closest);
			val m2 = nodeToMin(next);
			
			val nowMin = (now.getDay()*24 + now.getHours())*60 + now.getMinutes()
			println ("mins", m1,m2,nowMin)
			if (m2 > nowMin){ /// this is not working  - re evaluate
			  if (m2 > m1 ){next} 
			   
			  else {
			    closest}
			}
			else {closest}
		}
			
		@tailrec def closestAcc(closest :Node, sx:Seq[Node], now:Date): Node = {
		    println(closest)
			if (sx.isEmpty) closest else closestAcc(closestToNow(closest,sx.head,now), sx.tail,now)
		}
		
		getTemp(closestAcc(sx.head, sx.tail,Calendar.getInstance().getTime()))
	}    
  
	def addChild(xml:Node,label:String,child:Node):Node ={
	    new RuleTransformer(new AddChild(label,child)).transform(xml).head
	}
	
	def getTargetTemp ():Double = {
	  val xml =XML.loadFile(filename);
	  val smallest = lowest(xml  \ "target")
	  val largest = <target day = {((smallest  \ "@day").text.toInt+7).toString} hour={(smallest \ "@hour")} minute={(smallest \ "@minute")}>{smallest.text}</target>
	  println(largest)
	  val tgt = closest(addChild(xml,"schedule",largest) \ "target")
	  tgt
	}
}