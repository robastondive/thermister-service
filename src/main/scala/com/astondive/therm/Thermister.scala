package com.astondive.therm

import java.io.File
import scala.collection.JavaConversions._

object Thermister {
    val thermisterDir =  "\\sys\\bus\\w1\\devices"
      
    def recursiveListFiles(f: File): Array[File] = {
        val these = f.listFiles
        these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
    }
    
    def getFileTree(f: File): Stream[File] =
        f #:: (if (f.isDirectory) f.listFiles().toStream.flatMap(getFileTree) 
               else Stream.empty)

    def findDevice (f:java.io.File):String ={
    	val s = java.io.File.separator.toCharArray()
    	val a = f.getAbsolutePath().split(s(0))
    	a(a.length-2)
    }          
}

class Thermister {
    
	def getfiles() ={
	  Thermister.getFileTree(new File(Thermister.thermisterDir)).filter(_.getName.endsWith("w1-slave"))
	}
	
	def toXml():xml.Elem = {
	    <Thermisters> 
	    { 
	      getfiles.map(x =>  new ThermisterDevice(Thermister.findDevice(x),x.getAbsolutePath()).toXml() ) 
	    }
	    </Thermisters> 
	   
	}
}