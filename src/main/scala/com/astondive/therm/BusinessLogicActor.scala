package com.astondive.therm

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSelection.toScala
import akka.actor.actorRef2Scala
import scala.xml.Elem

case class Status(s: String)
case class BusinessTask(x: Elem)
case class Load(f: String)
case class Stats(s: String)



class BusinessLogicActor() extends Actor  with ActorLogging  {
	def receive = {
		case Stats(s) => {
			log.info ("no stats today")
			sender ! <stats>nothing to report</stats>
		}
		case Load(f) => {
			try{
				context.actorSelection("../ThermWebserviceActor")   ! Load (f)
			}
			catch{
				case e: Exception => log.error(s"Can't load $f") 
			}
		}
		case Status(s) => {
			sender ! s"OK $s"
		}
		case BusinessTask(x) => {	
			//x match {
			//   case <task>{contents}</task> => {
			//log.info("task: " + contents)
			val cmd = x \"@command"   
			val map  = x.attributes.asAttrMap

			log.info( cmd.toString())

			cmd.toString() match {
				case "find" =>{
				    log.info("find")
					val who  = (x \"@who").toString
					log.info( s"Find ${who.toString}")
					if (who.compareToIgnoreCase("all")==0){
						sender ! <task success="TRUE">{new Thermister().toXml}</task>
					}
					else
					{
						sender ! <task success="FALSE">Specific searches not implemented</task>
					}
				}
				case "scanTank" =>{
				    log.info("scan")
					val top  = (x \"@top").toString
					val middle  = (x \"@middle").toString
					val bottom  = (x \"@bottom").toString

					val thermister = new Thermister()
					val tank = new Tank(thermister.toXml)
				    if (!tank.reachedTarget){
				      // do something
				    }
				}
				case _ =>{
					log.info ("Task implemented not")
					sender ! <task success="FALSE">task not implemented</task>
				}
			}
		}
		case _ => {
			sender ! <estimate status="Failure" reason="task undefined"/> 
		}
	}
}
