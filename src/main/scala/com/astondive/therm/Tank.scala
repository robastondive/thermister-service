package com.astondive.therm

class Tank {
	var top = 0.0
	var bottom = 0.0
	var middle = 0.0

	def hasHotWater: Boolean = {
	   top > Configuration.hotWater
	}
	
	def needsStirring : Boolean = {
	  (top -10) > bottom
	}
	
	def temp : Double = {
	  (top + middle + bottom)/3
	}
	
	def reachedTarget : Boolean = {
	  temp > Configuration.waterTarget
	}
	
	def this ( xml : scala.xml.Elem) {
	  this()
	  this.parseXML(xml)
	}
	def parseXML ( xml : scala.xml.Elem) = {
	  for (block <- xml.map (t => t  \ "Thermister") ){
		  for ( item <- block){
		    println(item)
		    item match {
		      case <Thermister>{v}</Thermister> => {
		        val device = (item \ "@device")(0).toString
		        if(device.contentEquals(Configuration.top)) {
					top =v.toString.toDouble
		        }
				if(device.contentEquals(Configuration.bottom)) {
					bottom =v.toString.toDouble
				}
				if(device.contentEquals(Configuration.middle)) {
					middle =v.toString.toDouble
				}
		      }
		    }
		  }
		}
	}
}