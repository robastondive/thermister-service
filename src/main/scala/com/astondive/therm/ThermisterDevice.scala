package com.astondive.therm
import scala.io.Source

class ThermisterDevice(name:String,path:String) {
	def read() : Double ={
	   var s = new String("")
	   Source.fromFile(path).foreach(y => s=s+y)
       s.split("t=")(1).toDouble/1000.00
	}
	
	def toXml():scala.xml.Elem = {
	    val x =	     <Thermister device={name}>{read()}</Thermister>
	     println(x.toString())
	     x
	}
}