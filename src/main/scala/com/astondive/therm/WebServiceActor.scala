package com.astondive.therm

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

import org.mashupbots.socko.events.HttpResponseStatus
import org.mashupbots.socko.routes.GET
import org.mashupbots.socko.routes.HttpRequest
import org.mashupbots.socko.routes.Path
import org.mashupbots.socko.routes.PathSegments
import org.mashupbots.socko.routes.Routes
import org.mashupbots.socko.webserver.WebServer
import org.mashupbots.socko.webserver.WebServerConfig

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.pattern.ask
import akka.util.Timeout.durationToTimeout
 

class ActorMesg(val actor:Actor){}


class WebServiceActor extends Actor  with ActorLogging with WebServiceTrait{
	
    def actorRefFactory = context
    
    def logInfo(s:String) = { log.info(s)}
    def logError(s:String) = { log.error(s) }
    def getContext : akka.actor.ActorContext = { context}
	 /*
    implicit val timeout:akka.util.Timeout =1 seconds
   
	val routes =  Routes({
    
    	case HttpRequest(request) => request match {
     		case GET(PathSegments("find" :: name :: Nil)) => { 
    			log.info(s"calling task find for  :: $name ::Nil")
                val task = <task command="find" who={name}/>
    			try{   
    				val future = context.actorSelection("../ThermBusinessLogicActor")  ? 
    					new BusinessTask(task)
    				val res = Await.result(future, timeout.duration)
             		log.info(s"result of type ${res.getClass.getName()}")
             		request.response.write(res.asInstanceOf[xml.Elem].toString)
             	  		
  
    			
    			}
    			
    			catch{
            	  
    			  case jucte:java.util.concurrent.TimeoutException =>{
    			  	log.error("Timeout")
    			  }
            	}
             	
           	}
    		// If favicon.ico, just return a 404 because we don't have that file
    		case Path("/favicon.ico") => {
    			request.response.write(HttpResponseStatus.NOT_FOUND)
    		}
    		
    		case Path("/") =>{
    			val ap = Runtime.getRuntime().availableProcessors().toString.toLong
    			val fm = Runtime.getRuntime().freeMemory().toString.toLong/1024/1024
    			val tm = Runtime.getRuntime().totalMemory().toString.toLong/1024/1024
    			val mm = Runtime.getRuntime().maxMemory().toString.toLong/1024/1024
    			val um = tm - fm
    			
    			request.response.write(
    			<html>
                <body> 
                  <h1>Stats <i>Available Processors</i> on <i>{ap.toString}</i></h1>
                  <br/>
                  <h1>Stats <i>Free Memory</i> on <i>{fm.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Total Memory</i> on <i>{tm.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Used Memory</i> on <i>{um.toString}M</i></h1>
                  <br/>
                  <h1>Stats <i>Max Memory</i> on <i>{mm.toString}M</i></h1>
                </body>
              </html>.toString    
    			)
    		}
    	}
    })

    */
    
    
    val webServer = new WebServer(WebServerConfig(port=9000), routes, actorRefFactory)
    Runtime.getRuntime.addShutdownHook(new Thread {
    	override def run 
    	{ 
    		webServer.stop() 
    	}
    })
    
    
    
    
    def receive = {
    	case Load(f) => {
    	    try{
    	      webServer.stop()
    	      //load config file 
    	    }
    	    catch {
    	        case npe : java.lang.NullPointerException => log.info("Starting WebServer")
    	        case throwable: Throwable => log.info ("Restarting Webserver - new data loaded")
    	    }
    	    finally{
    	    	webServer.start()
    	    }
    	}
    	case xml:scala.xml.Elem =>{
    		log.info(s"Unrecognised ${xml.toString}")
    	}
    	  
    	case tcpConnect : akka.io.Tcp.Connected => println("tcp connection from"+tcpConnect.remoteAddress+" to "+tcpConnect.localAddress)
    	case other : 
    		Object => println("recieved obj"+other.getClass().toString())
  }
}




