package com.astondive.therm
import scala.xml._
import java.util.Calendar
import java.util.Date

import scala.annotation.tailrec
  

object xml {


   		
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  
     val x= <task command="find" who="me"/>       //> x  : scala.xml.Elem = <task command="find" who="me"/>
     
     
     val cmd = x \"@command"                      //> cmd  : scala.xml.NodeSeq = find
     val top= "28-0000036aff17"                   //> top  : String = 28-0000036aff17
     val bottom ="28-000003abc4a1"                //> bottom  : String = 28-000003abc4a1
     val middle = "28-000003abe0ae"               //> middle  : String = 28-000003abe0ae
     	var q:scala.xml.Node=null                 //> q  : scala.xml.Node = null
    		
     val xml = <Thermisters>
	    <Thermister device="10-000802824e58">30.0</Thermister><Thermister device="28-0000036aff17">30.125</Thermister><Thermister device="28-000003abc4a1">30.125</Thermister><Thermister device="28-000003abd06e">46.187</Thermister><Thermister device="28-000003abe0ae">30.187</Thermister>
	    </Thermisters>                        //> xml  : scala.xml.Elem = <Thermisters>
                                                  //| 	    <Thermister device="10-000802824e58">30.0</Thermister><Thermister de
                                                  //| vice="28-0000036aff17">30.125</Thermister><Thermister device="28-000003abc4a
                                                  //| 1">30.125</Thermister><Thermister device="28-000003abd06e">46.187</Thermiste
                                                  //| r><Thermister device="28-000003abe0ae">30.187</Thermister>
                                                  //| 	    </Thermisters>
    		val tank = new Tank(xml)          //> <Thermister device="10-000802824e58">30.0</Thermister>
                                                  //| <Thermister device="28-0000036aff17">30.125</Thermister>
                                                  //| <Thermister device="28-000003abc4a1">30.125</Thermister>
                                                  //| <Thermister device="28-000003abd06e">46.187</Thermister>
                                                  //| <Thermister device="28-000003abe0ae">30.187</Thermister>
                                                  //| tank  : com.astondive.therm.Tank = com.astondive.therm.Tank@2df5ccb3
				val t = new java.text.SimpleDateFormat()
                                                  //> t  : java.text.SimpleDateFormat = java.text.SimpleDateFormat@9fd35dda
       val c = java.util.Calendar.getInstance()   //> c  : java.util.Calendar = java.util.GregorianCalendar[time=1417732759218,are
                                                  //| FieldsSet=true,areAllFieldsSet=true,lenient=true,zone=sun.util.calendar.Zone
                                                  //| Info[id="Europe/London",offset=0,dstSavings=3600000,useDaylight=true,transit
                                                  //| ions=242,lastRule=java.util.SimpleTimeZone[id=Europe/London,offset=0,dstSavi
                                                  //| ngs=3600000,useDaylight=true,startYear=0,startMode=2,startMonth=2,startDay=-
                                                  //| 1,startDayOfWeek=1,startTime=3600000,startTimeMode=2,endMode=2,endMonth=9,en
                                                  //| dDay=-1,endDayOfWeek=1,endTime=3600000,endTimeMode=2]],firstDayOfWeek=2,mini
                                                  //| malDaysInFirstWeek=4,ERA=1,YEAR=2014,MONTH=11,WEEK_OF_YEAR=49,WEEK_OF_MONTH=
                                                  //| 1,DAY_OF_MONTH=4,DAY_OF_YEAR=338,DAY_OF_WEEK=5,DAY_OF_WEEK_IN_MONTH=1,AM_PM=
                                                  //| 1,HOUR=10,HOUR_OF_DAY=22,MINUTE=39,SECOND=19,MILLISECOND=218,ZONE_OFFSET=0,D
                                                  //| ST_OFFSET=0]
       val s= java.util.Calendar.SUNDAY           //> s  : Int = 1
       val m= java.util.Calendar.MONDAY           //> m  : Int = 2
       val th= java.util.Calendar.THURSDAY        //> th  : Int = 5
       val sat= java.util.Calendar.SATURDAY       //> sat  : Int = 7
     println(tank.top , tank.middle , tank.bottom)//> (30.125,46.187,0.0)
     
     val tgt1 = 	<target day = "1" hour="6" minute="0">35</target>
                                                  //> tgt1  : scala.xml.Elem = <target day="1" hour="6" minute="0">35</target>
  	 val tgt2 = 		<target day = "1" hour="9" minute="0">48</target>
                                                  //> tgt2  : scala.xml.Elem = <target day="1" hour="9" minute="0">48</target>
		   
     	val targetSchedule =  <schedule>
								<target day = "1" hour="6" minute="0">35</target>
								<target day = "2" hour="5" minute="0">35</target>
								<target day = "1" hour="9" minute="0">48</target>
								<target day = "2" hour="8" minute="0">48</target>
								<target day = "1" hour="16" minute="0">35</target>
								<target day = "2" hour="16" minute="0">35</target>
								<target day = "6" hour="20" minute="0">48</target>
								<target day = "7" hour="20" minute="0">48</target>
						  </schedule>
                                                  //> targetSchedule  : scala.xml.Elem = <schedule>
                                                  //| 								<target day="1" 
                                                  //| hour="6" minute="0">35</target>
                                                  //| 								<target day="2" 
                                                  //| hour="5" minute="0">35</target>
                                                  //| 								<target day="1" 
                                                  //| hour="9" minute="0">48</target>
                                                  //| 								<target day="2" 
                                                  //| hour="8" minute="0">48</target>
                                                  //| 								<target day="1" 
                                                  //| hour="16" minute="0">35</target>
                                                  //| 								<target day="2" 
                                                  //| hour="16" minute="0">35</target>
                                                  //| 								<target day="6" 
                                                  //| hour="20" minute="0">48</target>
                                                  //| 								<target day="7" 
                                                  //| hour="20" minute="0">48</target>
                                                  //| 						  </schedule>
                                              
  val today = Calendar.getInstance().getTime()    //> today  : java.util.Date = Thu Dec 04 22:39:19 GMT 2014
  println (today.getDay)                          //> 4
  
		def nodeToMin(n:Node): Int = {
				println(n)
				val min = ( n \ "@minute").text
				val hr = ( n \ "@hour").text
				val day = ( n \ "@day").text
				println(s"min $min hour $hr day $day")
				((day.toInt * 24) + hr.toInt) *60 + min.toInt
		}                                 //> nodeToMin: (n: scala.xml.Node)Int
		def closestToNow(closest:Node, next:Node, now:Date):Node = {
			val m1 = nodeToMin(closest);
			val m2 = nodeToMin(next);
			val nowMin = (now.getDay()*24 + now.getHours())*60 + now.getMinutes()
			
			if (m2 > nowMin)
				if (m2 < m1 ){  println("switching to"+m2) ; next}
				else { println(" staying with"+m1); closest }
			else {closest }
		}                                 //> closestToNow: (closest: scala.xml.Node, next: scala.xml.Node, now: java.uti
                                                  //| l.Date)scala.xml.Node
  
  
	def closest(sx:Seq[Node]): Double = {

		def getTemp(n:Node): Double = {
			n.text.toDouble
		}

		def closestToNow(closest:Node, next:Node, now:Date):Node = {
			val m1 = nodeToMin(closest);
			val m2 = nodeToMin(next);
			val nowMin = (now.getDay()*24 + now.getHours())*60 + now.getMinutes()
			
			if (m2 > nowMin)
				if (m2 < m1 ){  println("switching to"+m2) ; next}
				else { println(" staying with"+m1); closest }
			else {closest }
		}
			
    @tailrec def closestAcc(closest :Node, sx:Seq[Node], now:Date): Node = {
         println ("closest="+ closest)
         println ("sx="+ sx)
    
    	if (sx.isEmpty)
      	closest
      else{
         val clst = closestToNow(closest,sx.head,now)
         println ("clst="+ clst)
         closestAcc(clst, sx.tail,now)
      }
    }
    
    getTemp(closestAcc(sx.head, sx.tail,Calendar.getInstance().getTime()))
  }                                               //> closest: (sx: Seq[scala.xml.Node])Double

println(closestToNow(tgt1,tgt2,today))            //> <target day="1" hour="6" minute="0">35</target>
                                                  //| min 0 hour 6 day 1
                                                  //| <target day="1" hour="9" minute="0">48</target>
                                                  //| min 0 hour 9 day 1
                                                  //| <target day="1" hour="6" minute="0">35</target>

 val tgt = closest(targetSchedule \ "target")     //> closest=<target day="1" hour="6" minute="0">35</target>
                                                  //| sx=<target day="2" hour="5" minute="0">35</target><target day="1" hour="9" 
                                                  //| minute="0">48</target><target day="2" hour="8" minute="0">48</target><targe
                                                  //| t day="1" hour="16" minute="0">35</target><target day="2" hour="16" minute=
                                                  //| "0">35</target><target day="6" hour="20" minute="0">48</target><target day=
                                                  //| "7" hour="20" minute="0">48</target>
                                                  //| <target day="1" hour="6" minute="0">35</target>
                                                  //| min 0 hour 6 day 1
                                                  //| <target day="2" hour="5" minute="0">35</target>
                                                  //| min 0 hour 5 day 2
                                                  //| clst=<target day="1" hour="6" minute="0">35</target>
                                                  //| closest=<target day="1" hour="6" minute="0">35</target>
                                                  //| sx=<target day="1" hour="9" minute="0">48</target><target day="2" hour="8" 
                                                  //| minute="0">48</target><target day="1" hour="16" minute="0">35</target><targ
                                                  //| et day="2" hour="16" minute="0">35</target><target day="6" hour="20" minute
                                                  //| ="0">48</target><target day="7" hour="20" mi
                                                  //| Output exceeds cutoff limit.
println(tgt)                                      //> 35.0
def find(str:String, node :Node) : Node = {
	targetSchedule match {
		case n @ Elem (_,`str`,_,_,_*) =>  n
		case other => other
	}
	
	
}                                                 //> find: (str: String, node: scala.xml.Node)scala.xml.Node

 
	val sch = new Schedule("c:\\dev\\sched.xml")
                                                  //> sch  : com.astondive.therm.Schedule = com.astondive.therm.Schedule@2b0de40c
                                                  //| 
                                                 
 
 val target = sch. getTargetTemp ()               //> <target day="8" hour="6" minute="0">35</target>
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,3180,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,1980,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,3360,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,2400,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,3840,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,6960,7119)
                                                  //| <target minute="0" hour="6" day="1">35</target>
                                                  //| (mins,1800,8400,7119)
                                                  //| <target minute="0" hour="20" day="5">42</target>
                                                  //| (mins,8400,9840,7119)
                                                  //| <target minute="0" hour="20" day="6">48</target>
                                                  //| (mins,9840,11280,7119)
                                                  //| <target minute="0" hour="20" day="7">49</target>
                                                  //| (mins,11280,11880,7119)
                                                  //| <target day="8" hour="6" minute="0">35</target>
                                                  //| target  : Double = 35.0
}