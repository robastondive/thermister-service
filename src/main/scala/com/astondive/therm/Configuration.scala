package com.astondive.therm

import scala.xml._


object Configuration {
	val top="28-000003abc4a1"
	val middle="28-000003abd06e"
	val bottom="28-000003abe0aeid"
	  
	val hotWater = 42.0
	val waterTarget = 48.0
	
	
	/**
	 * The schedule works by looking for the next time slot and targeting that as
	 * the defined target for the tank, as soon as that time period passes then the
	 * next time becomes the new target
	 */
	
	def getTimeValue(itemNode: Node):Int = {
    	val min = (itemNode \ "@min").text.toInt*24*60
	    val hour = (itemNode \ "@hour").text.toInt*60
  	  	val day =   (itemNode \ "@day").text.toInt
    
     	day+hour+min
	}
	val targetSchedule =  <schedule>
								<target day = "1" hour="6" minute="0">35</target>
								<target day = "2" hour="5" minute="0">35</target>
								<target day = "3" hour="5" minute="0">35</target>
								<target day = "4" hour="5" minute="0">35</target>
								<target day = "5" hour="5" minute="0">35</target>
								<target day = "6" hour="5" minute="0">35</target>
								<target day = "7" hour="6" minute="0">35</target>

								<target day = "1" hour="9" minute="0">48</target>
								<target day = "2" hour="8" minute="0">48</target>
								<target day = "3" hour="8" minute="0">48</target>
								<target day = "4" hour="8" minute="0">48</target>
								<target day = "5" hour="8" minute="0">48</target>
								<target day = "6" hour="8" minute="0">48</target>
								<target day = "7" hour="9" minute="0">48</target>
								
								<target day = "1" hour="16" minute="0">35</target>
								<target day = "2" hour="16" minute="0">35</target>
								<target day = "3" hour="16" minute="0">35</target>
								<target day = "4" hour="16" minute="0">35</target>
								<target day = "5" hour="16" minute="0">35</target>
								<target day = "6" hour="16" minute="0">35</target>
								<target day = "7" hour="16" minute="0">35</target>

								<target day = "1" hour="20" minute="0">48</target>
								<target day = "2" hour="20" minute="0">48</target>
								<target day = "3" hour="20" minute="0">48</target>
								<target day = "4" hour="20" minute="0">48</target>
								<target day = "5" hour="20" minute="0">48</target>
								<target day = "6" hour="20" minute="0">48</target>
								<target day = "7" hour="20" minute="0">48</target>
						  </schedule>
}