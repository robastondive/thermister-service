import AssemblyKeys._

version       := "0.1"

assemblySettings

organization  := "com.astondive"

scalaVersion  := "2.10.3"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

mainClass in assembly := Some("com.astondive.Boot")

jarName in assembly := "webservice.jar"

libraryDependencies ++= {
  val akkaV = "2.2.0"
  val sockoV = "0.3.1"
  Seq(
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV,
    "org.mashupbots.socko" %% "socko-webserver" % sockoV,
    "org.specs2"          %%  "specs2"        % "2.2.3" % "test"
  )
}



