libraryDependencies <+= (sbtVersion)("org.scala-sbt" % "scripted-plugin" % _)

libraryDependencies += "org.mashupbots.socko" % "socko-webserver_2.10" % "0.3.1"

addSbtPlugin("com.github.gseitz" % "sbt-release" % "0.7.1")

addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.2.0")

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.4.0")

